Source: ddclient
Section: net
Priority: optional
Maintainer: Torsten Landschoff <torsten@debian.org>
Build-Depends: debhelper-compat (= 12), po-debconf, xmlto, quilt
Standards-Version: 3.9.8
Homepage: http://ddclient.net
Vcs-Git: https://salsa.debian.org/debian/ddclient.git
Vcs-Browser: https://salsa.debian.org/debian/ddclient

Package: ddclient
Architecture: all
Pre-Depends: ${misc:Pre-Depends}
Depends: perl, ${misc:Depends}, lsb-base (>= 3.1)
Recommends: libio-socket-ssl-perl
Provides: dyndns-client
Description: address updating utility for dynamic DNS services
 This package provides a client to update dynamic IP addresses with
 several dynamic DNS service providers, such as DynDNS.com.
 .
 This makes it possible to use a fixed hostname (such as
 myhost.dyndns.org) to access a machine with a dynamic IP address.
 .
 This client supports both dynamic and (near) static services, as  well as
 MX record and alternative name management. It caches the address, and
 only attempts the update when it has changed.
